export default [
  {
    title: "Eagle Wings National Site",
    href: "https://www.ewma-world.org/",
    picture: "eagleWingsLogo",
    resolutions: [1, 2, 3],
  },
  {
    title: "Illinois District",
    href: "http://www.ewma-ildistrict.org/",
    picture: "EWMA-District-Logo-Transparent",
    resolutions: [1, 2, 3],
  },
  {
    title: "Groupworks",
    href: "https://app.groupworks.com",
    picture: "groupworks",
    resolutions: [1, 2, 3],
  },
  {
    title: "Facebook",
    href: "https://www.facebook.com/ILChapterZ",
    picture: "fb",
    resolutions: [1, 2, 3],
  },
  {
    title: "Illinois Adventure",
    href: "http://www.illinoisadventuretv.org",
    picture: "illinoisAdventure",
    resolutions: [1, 2, 3],
  },
];
