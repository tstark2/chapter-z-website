export default [
  {
    title: "Chapter Director",
    name: "Debbie King",
    email: "cd@il-chapter-z.org",
    photo: "Debbie_King",
  },
  {
    title: "Treasurer",
    name: "Rhonda Morris",
    email: "treasurer@il-chapter-z.org",
    photo: "Rhonda_Morris",
  },
  {
    title: "Ride Coordinator",
    name: "Rich Morris",
    email: "rides@il-chapter-z.org",
    photo: "Rich_Morris",
  },
  {
    title: "Membership Development",
    name: "John & Julie Nixon",
    email: "mec@il-chapter-z.org",
    photo: "John_Julie_Nixon",
  },
  {
    title: "Assistant Ride Coordinator",
    name: "Brian King",
    email: "arc@il-chapter-z.org",
    photo: "Brian_King",
  },
  {
    title: "Newsletter Editor",
    name: "Randy House",
    email: "newsletter@il-chapter-z.org",
    photo: "waiting-on-photo",
  },
  {
    title: "Sunshine",
    name: "Melody Sledgister",
    email: "sunshine@il-chapter-z.org",
    photo: "waiting-on-photo",
  },
];
